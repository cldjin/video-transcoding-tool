import tkinter as tk
from tkinter import filedialog
import subprocess
import os
import shutil
from datetime import datetime

# 定义全局变量 selected_file
selected_file = ""


def open_wemb_file():
    global selected_file
    global ffmpeg_cmd
    selected_file = filedialog.askopenfilename(filetypes=[("Webm files", "*.webm")])
    print("Selected file:", selected_file)
    selected_file_name = os.path.basename(selected_file)
    selected_file_path = os.path.dirname(selected_file)
    file_name, file_extension = os.path.splitext(selected_file_name)
    now = datetime.now().strftime("_%Y%m%d%H%M%S")
    output_file_name = os.path.join(selected_file_path, file_name + now + ".mov")

    current_directory = os.getcwd()
    local_ffmpeg_cmd = os.path.join(current_directory, "ffmpeg\\ffmpeg.exe")

    if selected_file:
        # 清理上次残留的mov文件
        if os.path.exists(output_file_name):
            os.remove(output_file_name)

        # 解决文件路径带空格问题
        selected_file = f'"{selected_file}"'
        output_file_name = f'"{output_file_name}"'
        local_ffmpeg_cmd = f'"{local_ffmpeg_cmd}"'

        convert_audio(selected_file, "audio.m4a", local_ffmpeg_cmd)
        convert_frames(selected_file, "frames", local_ffmpeg_cmd)
        convert_frames_to_mov("frames", output_file_name, local_ffmpeg_cmd)

        # 清理过程文件
        if os.path.exists("audio.m4a"):
            os.remove("audio.m4a")
        if os.path.exists("temp.mov"):
            os.remove("temp.mov")
        force_delete_files("frames")


def force_delete_files(output_path):
    for item in os.listdir(output_path):
        item_path = os.path.join(output_path, item)
        if os.path.isfile(item_path):
            os.remove(item_path)
        elif os.path.isdir(item_path):
            shutil.rmtree(item_path)


def convert_audio(input_file, output_file, ffmpeg_cmd):
    command = f"{ffmpeg_cmd} -y -i {input_file} {output_file}"
    subprocess.run(command, shell=True)


def convert_frames(input_file, output_path, ffmpeg_cmd):
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    else:
        force_delete_files(output_path)
    output_file_pattern = os.path.join(output_path, 'frame%06d.png')
    command = f"{ffmpeg_cmd} -vcodec libvpx -i {input_file} {output_file_pattern}"
    subprocess.run(command, shell=True)


def convert_frames_to_mov(frame_path, mov_file, ffmpeg_cmd):
    frame_file_pattern = os.path.join(frame_path, 'frame%06d.png')
    command = f"{ffmpeg_cmd} -y -i {frame_file_pattern} -vcodec qtrle temp.mov"
    subprocess.run(command, shell=True)

    command = f"{ffmpeg_cmd} -y -i temp.mov -i audio.m4a -c:v copy -c:a aac -strict experimental {mov_file}"
    subprocess.run(command, shell=True)


root = tk.Tk()
root.title("Wemb转Mov")
root.geometry("300x200")  # 设置窗口大小为300x200像素

open_button = tk.Button(root, text="选择webm文件", command=open_wemb_file)
open_button.pack(side="bottom", pady=10)

root.mainloop()
